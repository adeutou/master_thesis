from javax.swing import JFrame, JButton, JTextArea, JScrollPane, JFileChooser, filechooser, ImageIcon
from java.awt import BorderLayout

class JythonGUI(JFrame):
    def __init__(self):
        super(JFrame, self).__init__("ODME - Scripting Tool")
        self.setSize(400, 300)
        self.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE)
        self.setLayout(BorderLayout())

        self.textArea = JTextArea()
        self.add(JScrollPane(self.textArea), BorderLayout.CENTER)

        self.button = JButton("Upload File", actionPerformed=self.upload_file)
        self.add(self.button, BorderLayout.SOUTH)

        # Set the icon for the JFrame
        self.set_icon()

    def set_icon(self):
            icon_path = "images/tu_clausthal_icon.jpg"
            window_icon = ImageIcon(self.getClass().getClassLoader().getResource(icon_path))
            self.setIconImage(window_icon.getImage())

    def upload_file(self, event):
        file_chooser = JFileChooser()
        filter = filechooser.FileNameExtensionFilter("XML and YAML files", ["xml", "yaml"])
        file_chooser.addChoosableFileFilter(filter)
        result = file_chooser.showOpenDialog(self)
        if result == JFileChooser.APPROVE_OPTION:
            selected_file = file_chooser.getSelectedFile()
            file_content = self.read_file(selected_file)
            self.textArea.setText(file_content)

    def read_file(self, file):
        with open(file.getAbsolutePath(), "r") as f:
            content = f.read()
        return content

if __name__ == "__main__":
    gui = JythonGUI()
    gui.setVisible(True)
