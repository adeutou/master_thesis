package scriptingtool;

import odme.odmeeditor.ODMEEditor;
import org.python.util.PythonInterpreter;
import org.python.core.*;
import org.yaml.snakeyaml.Yaml;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rsyntaxtextarea.SyntaxConstants;
import org.fife.ui.rtextarea.RTextScrollPane;

import javax.swing.*;
import javax.swing.filechooser.FileSystemView;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeSelectionModel;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.util.Map;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class ScriptingGUI extends JFrame {
    private RSyntaxTextArea codeEditorTextArea;
    private JTextArea pythonShellTextArea;
    private PythonInterpreter interpreter;
    private JTree fileExplorerTree;

    public ScriptingGUI() {
        setTitle("ODME - Scripting Tool");
        setSize(800, 600);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        // Load the TU Clausthal icon
        ImageIcon windowIcon = new ImageIcon(ODMEEditor.class.getClassLoader().getResource("images/tu_clausthal_icon.jpg"));
        setIconImage(windowIcon.getImage());

        // Initialize code editor
        setupCodeEditor();

        // Create the Python shell text area
        pythonShellTextArea = new JTextArea();
        pythonShellTextArea.setRows(10);
        pythonShellTextArea.setEditable(false);
        JScrollPane pythonShellScrollPane = new JScrollPane(pythonShellTextArea);

        // Create a split pane for code editor and Python shell
        JSplitPane rightSplitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, new RTextScrollPane(codeEditorTextArea), pythonShellScrollPane);
        rightSplitPane.setResizeWeight(0.75);

        // Create the file explorer
        DefaultMutableTreeNode root = new DefaultMutableTreeNode("Root");
        fileExplorerTree = new JTree(root);
        fileExplorerTree.setCellRenderer(new FileTreeCellRenderer());
        fileExplorerTree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
        JScrollPane fileExplorerScrollPane = new JScrollPane(fileExplorerTree);

        // Create a split pane for file explorer and the right split pane
        JSplitPane mainSplitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, fileExplorerScrollPane, rightSplitPane);
        mainSplitPane.setResizeWeight(0.25);

        // Set the main panel as the content pane
        setContentPane(mainSplitPane);

        // Create a menu bar
        JMenuBar menuBar = new JMenuBar();

        // Create "File" menu
        JMenu fileMenu = new JMenu("File");
        //sub-menu openFile
        JMenuItem openFileItem = new JMenuItem("Open File");
        openFileItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                openFile();
            }
        });
        fileMenu.add(openFileItem);
        //sub-menu save
        JMenuItem saveFileItem = new JMenuItem("Save");
        saveFileItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                saveFile();
            }
        });
        fileMenu.add(saveFileItem);

        menuBar.add(fileMenu);

        // Create "Run" menu
        JMenu runMenu = new JMenu("Run");
        JMenuItem runScriptItem = new JMenuItem("Run Script");
        runScriptItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                executeScript();
            }
        });
        runMenu.add(runScriptItem);
        menuBar.add(runMenu);

        // Set the menu bar
        setJMenuBar(menuBar);

        // Initialize Jython interpreter
        interpreter = new PythonInterpreter();

        pack();
        setLocationRelativeTo(null);
        setVisible(true);
    }

    private void setupCodeEditor() {
        // Create the RSyntaxTextArea for code editing
        codeEditorTextArea = new RSyntaxTextArea(20, 60);
        codeEditorTextArea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_PYTHON);
        codeEditorTextArea.setCodeFoldingEnabled(true);
        codeEditorTextArea.setText("# Start editing your Python script here...\n\n");

        // Additional setup for RSyntaxTextArea if needed
        codeEditorTextArea.setLineWrap(true);
        codeEditorTextArea.setWrapStyleWord(true);
    }

    private void openFile() {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
        int returnValue = fileChooser.showOpenDialog(this);
        if (returnValue == JFileChooser.APPROVE_OPTION) {
            File selectedFile = fileChooser.getSelectedFile();
            pythonShellTextArea.append("File opened: " + selectedFile.getAbsolutePath() + "\n");
            updateFileExplorerTree(selectedFile);

            // Determine the file type based on extension
            String extension = getFileExtension(selectedFile);
            if (extension.equalsIgnoreCase("yaml") || extension.equalsIgnoreCase("yml")) {
                displayYamlScript(selectedFile);
            } else if (extension.equalsIgnoreCase("xml")) {
                displayXmlScript(selectedFile);
            } else {
                pythonShellTextArea.append("Unsupported file type: " + extension + "\n");
            }
        }
    }

    private String getFileExtension(File file) {
        String fileName = file.getName();
        int lastDotIndex = fileName.lastIndexOf(".");
        if (lastDotIndex != -1 && lastDotIndex < fileName.length() - 1) {
            return fileName.substring(lastDotIndex + 1);
        }
        return "";
    }

    private void displayYamlScript(File file) {
        StringBuilder script = new StringBuilder();
        script.append("import yaml\n\n");
        script.append("# Load the YAML file\n");
        script.append("with open('").append(file.getAbsolutePath()).append("', 'r') as file:\n");
        script.append("    scenario_data = yaml.safe_load(file)\n\n");
        script.append("# Extract environment data\n");
        script.append("environment_data = scenario_data['Scenario']['Environment']\n");
        script.append("time_data = environment_data['Time'][0]['Hrs']\n");
        script.append("clouds_data = environment_data['Clouds']\n\n");
        script.append("# Extract entity data\n");
        script.append("entities_data = scenario_data['Scenario']['Entities']\n");
        script.append("tanker_aircraft_data = entities_data['TankerAircraft'][0]\n");
        script.append("receiver_aircraft_data = entities_data['ReceiverAircraft'][0]\n\n");
        script.append("# Prepare data for simulation\n");
        script.append("simulation_data = {\n");
        script.append("    'environment': {\n");
        script.append("        'time': time_data,\n");
        script.append("        'clouds': clouds_data\n");
        script.append("    },\n");
        script.append("    'entities': {\n");
        script.append("        'tanker_aircraft': {\n");
        script.append("            'altitude': tanker_aircraft_data['Altitude(ft)'],\n");
        script.append("            'heading': tanker_aircraft_data['Heading'],\n");
        script.append("            'speed': tanker_aircraft_data['Speed(kts)']\n");
        script.append("        },\n");
        script.append("        'receiver_aircraft': {\n");
        script.append("            'altitude': receiver_aircraft_data['Altitude(ft)'],\n");
        script.append("            'speed': receiver_aircraft_data['Speed(kts)'],\n");
        script.append("            'heading': receiver_aircraft_data['Heading']\n");
        script.append("        }\n");
        script.append("    }\n");
        script.append("}\n");

        codeEditorTextArea.setText(script.toString());
    }

    private void displayXmlScript(File file) {
        StringBuilder script = new StringBuilder();
        script.append("import xml.etree.ElementTree as ET\n\n");
        script.append("# Parse the XML file\n");
        script.append("tree = ET.parse('").append(file.getAbsolutePath()).append("')\n");
        script.append("root = tree.getroot()\n\n");
        script.append("# Extract environment data\n");
        script.append("environment = root.find('aspect[@name=\"scenarioDec\"]/entity[@name=\"Environment\"]/aspect[@name=\"envDec\"]')\n");
        script.append("time_data = environment.find('entity[@name=\"Time\"]/var[@name=\"Hrs\"]').attrib\n");
        script.append("clouds_data = [entity.attrib['name'] for entity in environment.findall('entity') if entity.attrib['name'] != 'Time']\n\n");
        script.append("# Extract entity data\n");
        script.append("entities = root.find('aspect[@name=\"scenarioDec\"]/entity[@name=\"Entities\"]/aspect[@name=\"node13Dec\"]')\n");
        script.append("tanker_aircraft = entities.find('entity[@name=\"TankerAircraft\"]')\n");
        script.append("tanker_aircraft_data = {\n");
        script.append("    'Altitude(ft)': tanker_aircraft.find('var[@name=\"Altitude(ft)\"]').attrib,\n");
        script.append("    'Heading': tanker_aircraft.find('var[@name=\"Heading\"]').attrib,\n");
        script.append("    'Speed(kts)': tanker_aircraft.find('var[@name=\"Speed(kts)\"]').attrib\n");
        script.append("}\n\n");
        script.append("receiver_aircraft = entities.find('entity[@name=\"ReceiverAircraft\"]')\n");
        script.append("receiver_aircraft_data = {\n");
        script.append("    'Altitude(ft)': receiver_aircraft.find('var[@name=\"Altitude(ft)\"]').attrib,\n");
        script.append("    'Speed(kts)': receiver_aircraft.find('var[@name=\"Speed(kts)\"]').attrib,\n");
        script.append("    'Heading': receiver_aircraft.find('var[@name=\"Heading\"]').attrib\n");
        script.append("}\n\n");
        script.append("# Prepare data for simulation\n");
        script.append("simulation_data = {\n");
        script.append("    'environment': {\n");
        script.append("        'time': time_data,\n");
        script.append("        'clouds': clouds_data\n");
        script.append("    },\n");
        script.append("    'entities': {\n");
        script.append("        'tanker_aircraft': tanker_aircraft_data,\n");
        script.append("        'receiver_aircraft': receiver_aircraft_data\n");
        script.append("    }\n");
        script.append("}\n");

        codeEditorTextArea.setText(script.toString());
    }

    private void updateFileExplorerTree(File file) {
        DefaultMutableTreeNode root = new DefaultMutableTreeNode(file);
        DefaultTreeModel model = new DefaultTreeModel(root);
        fileExplorerTree.setModel(model);

        if (file.isDirectory()) {
            populateTree(file, root);
        } else {
            populateFileStructure(file, root);
        }
    }

    private void populateTree(File file, DefaultMutableTreeNode node) {
        File[] files = file.listFiles();
        if (files != null) {
            for (File child : files) {
                DefaultMutableTreeNode childNode = new DefaultMutableTreeNode(child);
                node.add(childNode);
                if (child.isDirectory()) {
                    populateTree(child, childNode);
                }
            }
        }
    }

    private void populateFileStructure(File file, DefaultMutableTreeNode node) {
        try {
            if (file.getName().endsWith(".yaml") || file.getName().endsWith(".yml")) {
                parseYamlFile(file, node);
            } else if (file.getName().endsWith(".xml")) {
                parseXmlFile(file, node);
            } else {
                node.add(new DefaultMutableTreeNode("Unsupported file type"));
            }
        } catch (Exception e) {
            node.add(new DefaultMutableTreeNode("Error parsing file: " + e.getMessage()));
        }
    }

    private void parseYamlFile(File file, DefaultMutableTreeNode node) throws IOException {
        // Read the file and replace tabs with spaces
        StringBuilder content = new StringBuilder();
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = reader.readLine()) != null) {
                content.append(line.replace("\t", "    ")).append("\n");
            }
        }
        // Parse the sanitized YAML content
        Yaml yaml = new Yaml();
        Map<String, Object> yamlData = yaml.load(content.toString());
        addYamlNodes(yamlData, node);
    }

    private void addYamlNodes(Map<String, Object> yamlData, DefaultMutableTreeNode node) {
        for (Map.Entry<String, Object> entry : yamlData.entrySet()) {
            DefaultMutableTreeNode childNode = new DefaultMutableTreeNode(entry.getKey());
            node.add(childNode);
            if (entry.getValue() instanceof Map) {
                addYamlNodes((Map<String, Object>) entry.getValue(), childNode);
            } else {
                childNode.add(new DefaultMutableTreeNode(entry.getValue()));
            }
        }
    }

    private void parseXmlFile(File file, DefaultMutableTreeNode node) throws Exception {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document doc = builder.parse(file);
        Element rootElement = doc.getDocumentElement();
        DefaultMutableTreeNode rootNode = new DefaultMutableTreeNode(rootElement.getTagName());
        node.add(rootNode);
        addXmlNodes(rootElement, rootNode);
    }

    private void addXmlNodes(Element element, DefaultMutableTreeNode node) {
        NodeList nodeList = element.getChildNodes();
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node childNode = nodeList.item(i);
            if (childNode.getNodeType() == Node.ELEMENT_NODE) {
                Element childElement = (Element) childNode;
                DefaultMutableTreeNode childTreeNode = new DefaultMutableTreeNode(childElement.getTagName());
                node.add(childTreeNode);
                addXmlNodes(childElement, childTreeNode);
            }
        }
    }

    private void executeScript() {
        String script = codeEditorTextArea.getText();
        try (ByteArrayOutputStream out = new ByteArrayOutputStream();
             PrintStream printStream = new PrintStream(out)) {
            // Redirect the output and error streams of the interpreter
            PySystemState sys = Py.getSystemState();
            sys.stdout = new PyFile(printStream);
            sys.stderr = new PyFile(printStream);

            interpreter.exec(script);

            String result = out.toString();
            pythonShellTextArea.append(result);
            pythonShellTextArea.append("Script executed successfully.\n");
        } catch (Exception e) {
            pythonShellTextArea.append("Error executing script: " + e.getMessage() + "\n");
        }
    }

    private void saveFile() {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setDialogTitle("Save As");
        fileChooser.setSelectedFile(new File("script.py"));
        int userSelection = fileChooser.showSaveDialog(this);
        if (userSelection == JFileChooser.APPROVE_OPTION) {
            File fileToSave = fileChooser.getSelectedFile();
            try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileToSave))) {
                writer.write(codeEditorTextArea.getText());
                pythonShellTextArea.append("File saved: " + fileToSave.getAbsolutePath() + "\n");
            } catch (IOException e) {
                pythonShellTextArea.append("Error saving file: " + e.getMessage() + "\n");
            }
        }
    }

    // Custom cell renderer for file explorer tree
    class FileTreeCellRenderer extends DefaultTreeCellRenderer {
        private FileSystemView fileSystemView;

        public FileTreeCellRenderer() {
            fileSystemView = FileSystemView.getFileSystemView();
        }

        @Override
        public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded, boolean leaf, int row, boolean hasFocus) {
            JLabel label = (JLabel) super.getTreeCellRendererComponent(tree, value, selected, expanded, leaf, row, hasFocus);
            Object userObject = ((DefaultMutableTreeNode) value).getUserObject();
            if (userObject instanceof File) {
                File file = (File) userObject;
                label.setIcon(fileSystemView.getSystemIcon(file));
                label.setText(fileSystemView.getSystemDisplayName(file));
                label.setToolTipText(file.getPath());
            } else {
                label.setText(userObject.toString());
            }
            return label;
        }
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new ScriptingGUI();
            }
        });
    }
}
